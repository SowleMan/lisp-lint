This is a node module that will determine is a string's parentheses are properly nested.

To use the module:

cd to folder and run the command 

node index.js "(some string to process)"
