const processString = string => {
  let count = 0;

  for (let i = 0; i < string.length; i++) {
    if (string.charAt(i) === '(') {
      count++;
    } else if (string.charAt(i) === ')') {
      if (count === 0) {
        return false;
      }
      count--;
    }
  }
  return count === 0;
};

module.exports = {
  processString
};
